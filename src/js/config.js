$('.slider-home').slick({
    dots: true,
    fade: true,
    arrows: true,
    //autoplay:true,
    autoplaySpeed: 2500,
    speed: 900,
    infinite: true,
    //cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    //touchThreshold: 100
    prevArrow: '<img class="btn-carousel btn-prev" src="assets/images/icons/arrow-prev-home.svg">',
    nextArrow: '<img class="btn-carousel btn-next" src="assets/images/icons/arrow-next-home.svg">',
})